import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';

import DropDownList from './components/dropdown-list';

export default class App extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      items: [
        { key: 1, value: 'Incriminating' },
        { key: 2, value: 'Revealing' },
        { key: 3, value: 'Top Secret' },
        { key: 4, value: 'Doctype 4' },
        { key: 5, value: 'Doctype 5' },
        { key: 6, value: 'Doctype 6' },
        { key: 7, value: 'Doctype 7' },
        { key: 8, value: 'Doctype 8' },
        { key: 9, value: 'Doctype 9' },
        { key: 10, value: 'Doctype 10' },
        { key: 11, value: 'Doctype 11' },
        { key: 12, value: 'Doctype 12' },
        { key: 13, value: 'Doctype 13' },
        { key: 14, value: 'Doctype 14' },
        { key: 15, value: 'Doctype 15' },
        { key: 16, value: 'Doctype 16' },
        { key: 17, value: 'Doctype 17' },
        { key: 18, value: 'Doctype 18' },
      ],
  
      selectedItem: { key: 0, value: '' },
    };
  }

  updateState = (item) => {
    this.setState({
      selectedItem: item
    });
}

  render() {
    return (
      <View style={styles.container}>
        <DropDownList dropdownItems={this.state.items} updateState={this.updateState} dark={true} />
      </View>      
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 50,
    backgroundColor: '#13223c',//'#fff',//
    height: 800
 },
});
