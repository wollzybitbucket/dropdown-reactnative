import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TouchableHighlight, Modal, FlatList, Dimensions } from 'react-native';

import FontAwesome from 'react-native-vector-icons/FontAwesome';

export default class DropDownList extends React.Component {

    constructor(props) {
        super(props);

        this.state = {  
            isModalVisible: false,

            buttonY: 0,
            buttonHeight: 0,

            popupY: 0,
            popupHeight: 0,

            selectedItem: { key: 0, value: 'Choose type of document...' },

            fontLoaded: false,
        };
    }    

    async componentWillMount() {
        await Expo.Font.loadAsync({
            'SFProDisplayMedium': require('../assets/fonts/SFProDisplayMedium.otf'),
        });
        this.setState({ fontLoaded: true });
    }

    view = null;    

    measureView = () => {
        this.view.measure((fx, fy, width, height, px, py) => {
          this.setState({buttonY: py});
          this.setState({buttonHeight: height});          
        });
    }

    calcPopup = () => {
        const dimensions = Dimensions.get('window');
        const windowHeight = dimensions.height;
        const top = this.state.buttonY + this.state.buttonHeight/2 + 25; 
        const itemsHeight = 42 * Object.keys(this.props.dropdownItems).length;
        const frameHeight = windowHeight - this.state.buttonY - this.state.buttonHeight - 30;
        const height = (itemsHeight < frameHeight) ? itemsHeight : frameHeight;
        this.setState({popupY: top});
        this.setState({popupHeight: height});
    }
      
    render() {
      return (
        
        <View>
            {this.state.fontLoaded ? (  
            <View>        
                <View style={styles.dropdownHolder}>
                    <Text style={{color: '#868dab', fontSize: 15, marginLeft: 10, height: 20, fontFamily: 'SFProDisplayMedium' }}>Type of document:</Text>
                    <TouchableOpacity onPress={() => { this.calcPopup(); this.setState({isModalVisible: true})}}>
                        <View style={styles.selectedItemContainer} ref={(view) => { this.view = view }} onLayout={this.measureView}>
                            <Text style={{color:'#bbc2ce', fontSize: 18, fontFamily: 'SFProDisplayMedium' }}>{this.state.selectedItem.value}</Text>
                            <FontAwesome name='angle-down' style={{fontSize: 35, color: '#bbc2ce', marginRight: 15 }} />
                        </View>
                    </TouchableOpacity>
                </View>
                <Modal visible={this.state.isModalVisible} transparent={true} supportedOrientations={['portrait', 'landscape']}>
                    <View style={{ height: this.state.popupHeight, marginLeft: 10, marginTop: this.state.popupY, width:390, borderWidth: 1, 
            borderColor: '#868dab' }}>
                    <FlatList style={{ flexGrow: 0, height: this.state.popupHeight, width:370 }} 
                        data={this.props.dropdownItems} renderItem={({item})=> (
                            <TouchableHighlight key={item.key.toString()} style={{width: 350, height: 41 }} underlayColor={this.props.dark?'#2c5d97':'#e7f2ff'}
                            onPress={() => {this.setState({isModalVisible: false}); this.setState({selectedItem: item}); this.props.updateState(item); }}>
                                <View style={styles.itemContainer}>
                                    <Text style={styles.itemText}>{item.value}</Text>
                                </View>
                            </TouchableHighlight>  
                        )}
                    />
                    </View>
                </Modal>
            </View>
            ): null}      
        </View>
        
      );
    }
  }
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    dropdownHolder: {
        width: 410, 
        height: 58, 
    },
    itemText: {
        color: '#868dab', 
        fontSize: 18,
    },
    itemSelectedText: {
        color: '#fff', 
        fontSize: 18,
    },
    selectedItemContainer : {
        height: 62, 
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center', 
        borderBottomWidth: 1, 
        borderBottomColor: '#bec0cd',
        marginTop: 20, 
        marginLeft: 10, 
        marginRight: 10,
    },
    itemContainer : {
        marginTop: 8, 
        marginLeft: 10, 
    }
  });
  